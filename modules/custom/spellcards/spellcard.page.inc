<?php

/**
 * @file
 * Contains spellcard.page.inc..
 *
 * Page callback for Spellcard entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Spellcard templates.
 *
 * Default template: spellcard.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_spellcard(array &$variables) {
  // Fetch Spellcard Entity Object.
  $spellcard = $variables['elements']['#spellcard'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
