<?php

/**
 * @file
 * Contains \Drupal\spellcards\SpellcardInterface.
 */

namespace Drupal\spellcards;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Spellcard entities.
 *
 * @ingroup spellcards
 */
interface SpellcardInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {
  // Add get/set methods for your configuration properties here.

}
