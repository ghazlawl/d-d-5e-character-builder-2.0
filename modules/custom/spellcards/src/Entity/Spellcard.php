<?php

/**
 * @file
 * Contains \Drupal\spellcards\Entity\Spellcard.
 */

namespace Drupal\spellcards\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\spellcards\SpellcardInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Spellcard entity.
 *
 * @ingroup spellcards
 *
 * @ContentEntityType(
 *   id = "spellcard",
 *   label = @Translation("Spellcard"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\spellcards\SpellcardListBuilder",
 *     "views_data" = "Drupal\spellcards\Entity\SpellcardViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\spellcards\Entity\Form\SpellcardForm",
 *       "add" = "Drupal\spellcards\Entity\Form\SpellcardForm",
 *       "edit" = "Drupal\spellcards\Entity\Form\SpellcardForm",
 *       "delete" = "Drupal\spellcards\Entity\Form\SpellcardDeleteForm",
 *     },
 *     "access" = "Drupal\spellcards\SpellcardAccessControlHandler",
 *   },
 *   base_table = "spellcard",
 *   admin_permission = "administer Spellcard entity",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/spellcard/{spellcard}",
 *     "edit-form" = "/admin/spellcard/{spellcard}/edit",
 *     "delete-form" = "/admin/spellcard/{spellcard}/delete"
 *   },
 *   field_ui_base_route = "spellcard.settings"
 * )
 */
class Spellcard extends ContentEntityBase implements SpellcardInterface {
  use EntityChangedTrait;
  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += array(
      'user_id' => \Drupal::currentUser()->id(),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {

    /* ======================================== */
    /* Default Fields
    /* ======================================== */

    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('The ID of the Spellcard entity.'))
      ->setReadOnly(TRUE);

    $fields['uuid'] = BaseFieldDefinition::create('uuid')
      ->setLabel(t('UUID'))
      ->setDescription(t('The UUID of the Spellcard entity.'))
      ->setReadOnly(TRUE);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the Spellcard entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDefaultValueCallback('Drupal\node\Entity\Node::getCurrentUserId')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', array(
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => array(
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ),
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the Spellcard entity.'))
      ->setSettings(array(
        'max_length' => 50,
        'text_processing' => 0,
      ))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'weight' => -4,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['langcode'] = BaseFieldDefinition::create('language')
      ->setLabel(t('Language code'))
      ->setDescription(t('The language code for the Spellcard entity.'));

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    /* ======================================== */
    /* Custom Fields Below
    /* ======================================== */

    $fields['description'] = BaseFieldDefinition::create('text_long')
        ->setLabel(t('Description'))
        ->setDisplayOptions('view', array(
            'label' => 'above',
            'type' => 'string_textarea',
            'weight' => -3,

        ))
        ->setDisplayOptions('form', array(
            'type' => 'string_textarea',
            'weight' => -3,
            'settings' => array(
                'placeholder' => '',
            ),
        ))
        ->setDisplayConfigurable('view', TRUE)
        ->setDisplayConfigurable('form', TRUE);

    /*
    $fields['casting_time'] = BaseFieldDefinition::create('string')
        ->setLabel(t('Casting Time'))
        ->setSettings(array(
            'max_length' => 50,
            'text_processing' => 0,
        ))
        ->setDefaultValue('')
        ->setDisplayOptions('view', array(
            'label' => 'above',
            'type' => 'string',
            'weight' => -2,
        ))
        ->setDisplayOptions('form', array(
            'type' => 'string_textfield',
            'weight' => -2,
        ))
        ->setDisplayConfigurable('form', TRUE)
        ->setDisplayConfigurable('view', TRUE);
    */

    return $fields;
  }

}
