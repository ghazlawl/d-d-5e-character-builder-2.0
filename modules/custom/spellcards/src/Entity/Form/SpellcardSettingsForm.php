<?php

/**
 * @file
 * Contains \Drupal\spellcards\Entity\Form\SpellcardSettingsForm.
 */

namespace Drupal\spellcards\Entity\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class SpellcardSettingsForm.
 *
 * @package Drupal\spellcards\Form
 *
 * @ingroup spellcards
 */
class SpellcardSettingsForm extends FormBase {
  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'Spellcard_settings';
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Empty implementation of the abstract submit class.
  }


  /**
   * Defines the settings form for Spellcard entities.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   Form definition array.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['Spellcard_settings']['#markup'] = 'Settings form for Spellcard entities. Manage field settings here.';
    return $form;
  }

}
