<?php

/**
 * @file
 * Contains \Drupal\spellcards\Entity\Spellcard.
 */

namespace Drupal\spellcards\Entity;

use Drupal\views\EntityViewsData;
use Drupal\views\EntityViewsDataInterface;

/**
 * Provides Views data for Spellcard entities.
 */
class SpellcardViewsData extends EntityViewsData implements EntityViewsDataInterface {
  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['spellcard']['table']['base'] = array(
      'field' => 'id',
      'title' => $this->t('Spellcard'),
      'help' => $this->t('The Spellcard ID.'),
    );

    return $data;
  }

}
