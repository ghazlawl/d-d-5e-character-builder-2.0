<?php

/**
 * @file
 * Contains \Drupal\spellcards\SpellcardListBuilder.
 */

namespace Drupal\spellcards;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Routing\LinkGeneratorTrait;
use Drupal\Core\Url;

/**
 * Defines a class to build a listing of Spellcard entities.
 *
 * @ingroup spellcards
 */
class SpellcardListBuilder extends EntityListBuilder {
  use LinkGeneratorTrait;
  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Spellcard ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\spellcards\Entity\Spellcard */
    $row['id'] = $entity->id();
    $row['name'] = $this->l(
      $this->getLabel($entity),
      new Url(
        'entity.spellcard.edit_form', array(
          'spellcard' => $entity->id(),
        )
      )
    );
    return $row + parent::buildRow($entity);
  }

}
