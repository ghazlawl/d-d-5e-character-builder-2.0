<?php

/**
 * @file
 * Contains \Drupal\spellcards\SpellcardAccessControlHandler.
 */

namespace Drupal\spellcards;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Spellcard entity.
 *
 * @see \Drupal\spellcards\Entity\Spellcard.
 */
class SpellcardAccessControlHandler extends EntityAccessControlHandler {
  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {

    switch ($operation) {
      case 'view':
        return AccessResult::allowedIfHasPermission($account, 'view spellcard entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit spellcard entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete spellcard entities');
    }

    return AccessResult::allowed();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add spellcard entities');
  }

}
