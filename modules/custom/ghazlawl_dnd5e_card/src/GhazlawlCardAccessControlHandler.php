<?php

/**
 * @file
 * Contains \Drupal\ghazlawl_dnd5e_card\GhazlawlCardAccessControlHandler.
 */

namespace Drupal\ghazlawl_dnd5e_card;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Ghazlawl Card entity.
 *
 * @see \Drupal\ghazlawl_dnd5e_card\Entity\GhazlawlCard.
 */
class GhazlawlCardAccessControlHandler extends EntityAccessControlHandler {
  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {

    switch ($operation) {
      case 'view':
        return AccessResult::allowedIfHasPermission($account, 'view ghazlawl card entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit ghazlawl card entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete ghazlawl card entities');
    }

    return AccessResult::allowed();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add ghazlawl card entities');
  }

}
