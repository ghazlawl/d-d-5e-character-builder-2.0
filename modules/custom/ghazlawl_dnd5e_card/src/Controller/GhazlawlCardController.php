<?php

/**
 * @file
 * Contains \Drupal\ghazlawl_dnd5e_card\Controller\GhazlawlCardController.
 */

namespace Drupal\ghazlawl_dnd5e_card\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Class GhazlawlCardController.
 *
 * @package Drupal\ghazlawl_dnd5e_card\Controller
 */
class GhazlawlCardController extends ControllerBase {
  /**
   * Add.
   *
   * @return string
   *   Return Hello string.
   */
  public function add($param_1, $param_2) {
    return [
        '#type' => 'markup',
        '#markup' => $this->t('Implement method: add with parameter(s): $param_1, $param_2')
    ];
  }

}
