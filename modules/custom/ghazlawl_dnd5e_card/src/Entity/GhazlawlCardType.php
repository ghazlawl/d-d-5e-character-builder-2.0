<?php

/**
 * @file
 * Contains \Drupal\ghazlawl_dnd5e_card\Entity\GhazlawlCardType.
 */

namespace Drupal\ghazlawl_dnd5e_card\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\ghazlawl_dnd5e_card\GhazlawlCardTypeInterface;

/**
 * Defines the Ghazlawl Card Type entity.
 *
 * @ConfigEntityType(
 *   id = "ghazlawl_card_type",
 *   label = @Translation("Ghazlawl Card Type"),
 *   handlers = {
 *     "list_builder" = "Drupal\ghazlawl_dnd5e_card\GhazlawlCardTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\ghazlawl_dnd5e_card\Form\GhazlawlCardTypeForm",
 *       "edit" = "Drupal\ghazlawl_dnd5e_card\Form\GhazlawlCardTypeForm",
 *       "delete" = "Drupal\ghazlawl_dnd5e_card\Form\GhazlawlCardTypeDeleteForm"
 *     }
 *   },
 *   config_prefix = "ghazlawl_card_type",
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/ghazlawl_card_type/{ghazlawl_card_type}",
 *     "edit-form" = "/admin/structure/ghazlawl_card_type/{ghazlawl_card_type}/edit",
 *     "delete-form" = "/admin/structure/ghazlawl_card_type/{ghazlawl_card_type}/delete",
 *     "collection" = "/admin/structure/visibility_group"
 *   }
 * )
 */
class GhazlawlCardType extends ConfigEntityBase implements GhazlawlCardTypeInterface {
  /**
   * The Ghazlawl Card Type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Ghazlawl Card Type label.
   *
   * @var string
   */
  protected $label;

}
