<?php

/**
 * @file
 * Contains \Drupal\ghazlawl_dnd5e_card\Entity\GhazlawlCard.
 */

namespace Drupal\ghazlawl_dnd5e_card\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\ghazlawl_dnd5e_card\GhazlawlCardInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Ghazlawl Card entity.
 *
 * @ingroup ghazlawl_dnd5e_card
 *
 * @ContentEntityType(
 *   id = "ghazlawl_card",
 *   label = @Translation("Ghazlawl Card"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\ghazlawl_dnd5e_card\GhazlawlCardListBuilder",
 *     "views_data" = "Drupal\ghazlawl_dnd5e_card\Entity\GhazlawlCardViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\ghazlawl_dnd5e_card\Entity\Form\GhazlawlCardForm",
 *       "add" = "Drupal\ghazlawl_dnd5e_card\Entity\Form\GhazlawlCardForm",
 *       "edit" = "Drupal\ghazlawl_dnd5e_card\Entity\Form\GhazlawlCardForm",
 *       "delete" = "Drupal\ghazlawl_dnd5e_card\Entity\Form\GhazlawlCardDeleteForm",
 *     },
 *     "access" = "Drupal\ghazlawl_dnd5e_card\GhazlawlCardAccessControlHandler",
 *   },
 *   base_table = "ghazlawl_card",
 *   admin_permission = "administer GhazlawlCard entity",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/ghazlawl_card/{ghazlawl_card}",
 *     "edit-form" = "/admin/ghazlawl_card/{ghazlawl_card}/edit",
 *     "delete-form" = "/admin/ghazlawl_card/{ghazlawl_card}/delete"
 *   },
 *   field_ui_base_route = "ghazlawl_card.settings"
 * )
 */
class GhazlawlCard extends ContentEntityBase implements GhazlawlCardInterface {
  use EntityChangedTrait;
  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += array(
      'user_id' => \Drupal::currentUser()->id(),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('The ID of the Ghazlawl Card entity.'))
      ->setReadOnly(TRUE);

    $fields['uuid'] = BaseFieldDefinition::create('uuid')
      ->setLabel(t('UUID'))
      ->setDescription(t('The UUID of the Ghazlawl Card entity.'))
      ->setReadOnly(TRUE);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the Ghazlawl Card entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDefaultValueCallback('Drupal\node\Entity\Node::getCurrentUserId')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', array(
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => array(
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ),
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the Ghazlawl Card entity.'))
      ->setSettings(array(
        'max_length' => 50,
        'text_processing' => 0,
      ))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'weight' => -4,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['langcode'] = BaseFieldDefinition::create('language')
      ->setLabel(t('Language code'))
      ->setDescription(t('The language code for the Ghazlawl Card entity.'));

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

}
