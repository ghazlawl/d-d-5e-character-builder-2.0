<?php

/**
 * @file
 * Contains \Drupal\ghazlawl_dnd5e_card\Entity\Form\GhazlawlCardForm.
 */

namespace Drupal\ghazlawl_dnd5e_card\Entity\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\Language;

/**
 * Form controller for Ghazlawl Card edit forms.
 *
 * @ingroup ghazlawl_dnd5e_card
 */
class GhazlawlCardForm extends ContentEntityForm {
  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /* @var $entity \Drupal\ghazlawl_dnd5e_card\Entity\GhazlawlCard */
    $form = parent::buildForm($form, $form_state);
    $entity = $this->entity;

    $form['langcode'] = array(
      '#title' => $this->t('Language'),
      '#type' => 'language_select',
      '#default_value' => $entity->langcode->value,
      '#languages' => Language::STATE_ALL,
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submit(array $form, FormStateInterface $form_state) {
    // Build the entity object from the submitted values.
    $entity = parent::submit($form, $form_state);

    return $entity;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;
    $status = $entity->save();

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created the %label Ghazlawl Card.', [
          '%label' => $entity->label(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved the %label Ghazlawl Card.', [
          '%label' => $entity->label(),
        ]));
    }
    $form_state->setRedirect('entity.ghazlawl_card.edit_form', ['ghazlawl_card' => $entity->id()]);
  }

}
