<?php

/**
 * @file
 * Contains \Drupal\ghazlawl_dnd5e_card\Entity\Form\GhazlawlCardSettingsForm.
 */

namespace Drupal\ghazlawl_dnd5e_card\Entity\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class GhazlawlCardSettingsForm.
 *
 * @package Drupal\ghazlawl_dnd5e_card\Form
 *
 * @ingroup ghazlawl_dnd5e_card
 */
class GhazlawlCardSettingsForm extends FormBase {
  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'GhazlawlCard_settings';
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Empty implementation of the abstract submit class.
  }


  /**
   * Defines the settings form for Ghazlawl Card entities.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   Form definition array.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['GhazlawlCard_settings']['#markup'] = 'Settings form for Ghazlawl Card entities. Manage field settings here.';
    return $form;
  }

}
