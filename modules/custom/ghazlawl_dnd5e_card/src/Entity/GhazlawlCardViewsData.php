<?php

/**
 * @file
 * Contains \Drupal\ghazlawl_dnd5e_card\Entity\GhazlawlCard.
 */

namespace Drupal\ghazlawl_dnd5e_card\Entity;

use Drupal\views\EntityViewsData;
use Drupal\views\EntityViewsDataInterface;

/**
 * Provides Views data for Ghazlawl Card entities.
 */
class GhazlawlCardViewsData extends EntityViewsData implements EntityViewsDataInterface {
  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['ghazlawl_card']['table']['base'] = array(
      'field' => 'id',
      'title' => $this->t('Ghazlawl Card'),
      'help' => $this->t('The Ghazlawl Card ID.'),
    );

    return $data;
  }

}
