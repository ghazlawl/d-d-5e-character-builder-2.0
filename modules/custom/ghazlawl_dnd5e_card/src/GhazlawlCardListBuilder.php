<?php

/**
 * @file
 * Contains \Drupal\ghazlawl_dnd5e_card\GhazlawlCardListBuilder.
 */

namespace Drupal\ghazlawl_dnd5e_card;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Routing\LinkGeneratorTrait;
use Drupal\Core\Url;

/**
 * Defines a class to build a listing of Ghazlawl Card entities.
 *
 * @ingroup ghazlawl_dnd5e_card
 */
class GhazlawlCardListBuilder extends EntityListBuilder {
  use LinkGeneratorTrait;
  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Ghazlawl Card ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\ghazlawl_dnd5e_card\Entity\GhazlawlCard */
    $row['id'] = $entity->id();
    $row['name'] = $this->l(
      $this->getLabel($entity),
      new Url(
        'entity.ghazlawl_card.edit_form', array(
          'ghazlawl_card' => $entity->id(),
        )
      )
    );
    return $row + parent::buildRow($entity);
  }

}
