<?php

/**
 * @file
 * Contains \Drupal\ghazlawl_dnd5e_card\GhazlawlCardTypeInterface.
 */

namespace Drupal\ghazlawl_dnd5e_card;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Ghazlawl Card Type entities.
 */
interface GhazlawlCardTypeInterface extends ConfigEntityInterface {
  // Add get/set methods for your configuration properties here.

}
