<?php

/**
 * @file
 * Contains \Drupal\ghazlawl_dnd5e_card\GhazlawlCardInterface.
 */

namespace Drupal\ghazlawl_dnd5e_card;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Ghazlawl Card entities.
 *
 * @ingroup ghazlawl_dnd5e_card
 */
interface GhazlawlCardInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {
  // Add get/set methods for your configuration properties here.

}
