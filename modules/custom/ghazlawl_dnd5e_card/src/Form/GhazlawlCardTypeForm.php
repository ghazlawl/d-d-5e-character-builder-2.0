<?php

/**
 * @file
 * Contains \Drupal\ghazlawl_dnd5e_card\Form\GhazlawlCardTypeForm.
 */

namespace Drupal\ghazlawl_dnd5e_card\Form;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class GhazlawlCardTypeForm.
 *
 * @package Drupal\ghazlawl_dnd5e_card\Form
 */
class GhazlawlCardTypeForm extends EntityForm {
  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $ghazlawl_card_type = $this->entity;
    $form['label'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $ghazlawl_card_type->label(),
      '#description' => $this->t("Label for the Ghazlawl Card Type."),
      '#required' => TRUE,
    );

    $form['id'] = array(
      '#type' => 'machine_name',
      '#default_value' => $ghazlawl_card_type->id(),
      '#machine_name' => array(
        'exists' => '\Drupal\ghazlawl_dnd5e_card\Entity\GhazlawlCardType::load',
      ),
      '#disabled' => !$ghazlawl_card_type->isNew(),
    );

    /* You will need additional form elements for your custom properties. */

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $ghazlawl_card_type = $this->entity;
    $status = $ghazlawl_card_type->save();

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created the %label Ghazlawl Card Type.', [
          '%label' => $ghazlawl_card_type->label(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved the %label Ghazlawl Card Type.', [
          '%label' => $ghazlawl_card_type->label(),
        ]));
    }
    $form_state->setRedirectUrl($ghazlawl_card_type->urlInfo('collection'));
  }

}
