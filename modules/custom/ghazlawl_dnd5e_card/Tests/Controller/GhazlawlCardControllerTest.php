<?php

/**
 * @file
 * Contains \Drupal\ghazlawl_dnd5e_card\Tests\GhazlawlCardController.
 */

namespace Drupal\ghazlawl_dnd5e_card\Tests;

use Drupal\simpletest\WebTestBase;

/**
 * Provides automated tests for the ghazlawl_dnd5e_card module.
 */
class GhazlawlCardControllerTest extends WebTestBase {
  /**
   * {@inheritdoc}
   */
  public static function getInfo() {
    return array(
      'name' => "ghazlawl_dnd5e_card GhazlawlCardController's controller functionality",
      'description' => 'Test Unit for module ghazlawl_dnd5e_card and controller GhazlawlCardController.',
      'group' => 'Other',
    );
  }

  /**
   * {@inheritdoc}
   */
  public function setUp() {
    parent::setUp();
  }

  /**
   * Tests ghazlawl_dnd5e_card functionality.
   */
  public function testGhazlawlCardController() {
    // Check that the basic functions of module ghazlawl_dnd5e_card.
    $this->assertEquals(TRUE, TRUE, 'Test Unit Generated via App Console.');
  }

}
