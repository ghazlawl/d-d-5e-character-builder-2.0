<?php

/**
 * @file
 * Contains ghazlawl_card.page.inc..
 *
 * Page callback for Ghazlawl Card entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Ghazlawl Card templates.
 *
 * Default template: ghazlawl_card.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_ghazlawl_card(array &$variables) {
  // Fetch GhazlawlCard Entity Object.
  $ghazlawl_card = $variables['elements']['#ghazlawl_card'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
